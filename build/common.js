import fs from "fs";
import {join, dirname} from "path";
import {fileURLToPath} from "url";
import {spawnSync} from "child_process";

export const TEMP_DIR = join(dirname(fileURLToPath(import.meta.url)), "..", "./.build-temp");
const COL_CLEAR = "\u001b[0m";
const COL_CYAN = "\u001b[36m";
const COL_RED = "\u001b[31m";

export function printHeadline(headline) {
  if (headline) {
    process.stdout.write(`${COL_CYAN}${"-".repeat(14 + headline.length)}\n`);
    process.stdout.write(`${COL_CYAN}=== ${COL_CLEAR}MLAF: ${headline} ${COL_CYAN}===\n`);
    process.stdout.write(`${COL_CYAN}${"-".repeat(14 + headline.length)}\n`);
  }
}

export function printProgress(text) {
  if (text)
    process.stdout.write(`${COL_CYAN}* MLAF: ${COL_CLEAR}${text}\n`);
}

export function exec(command, args, cwd) {
  if (command) {
    try {
      let ret = spawnSync(command, args, {shell: true, stdio: "inherit", cwd});
      if (ret.status !== 0)
        die("Error during subprocess");
    }
    catch (err) {
      die(err);
    }
  }
}

export function cleanUpTemp() {
  fs.rmSync(TEMP_DIR, {recursive: true, force: true});
}

export function createTemp() {
  fs.mkdirSync(TEMP_DIR);
}

export function die(err) {
  if (err)
    process.stdout.write(`${COL_RED}ERROR:${COL_CLEAR} ${err}\n`);
  cleanUpTemp();
  process.exit(1);
}
