import fs from "fs";
import {printHeadline, printProgress, exec, cleanUpTemp, createTemp, TEMP_DIR} from "./common.js";

// `npm run build-abp` to build ABP with the local version of mlaf
// `npm run build-abp -- --remote` to build ABP with `@eyeo/mlaf` from npmjs. Useful to test ABP after publishing

const REMOTE = process.argv && process.argv.includes("--remote");
const MLAF_ORIGIN = REMOTE ? "@eyeo/mlaf" : "../..";
const SNIPPETS_ORIGIN = "git@gitlab.com:eyeo/anti-cv/snippets.git";
const SNIPPETS_TAG = "main";
const ABP_ORIGIN = "git@gitlab.com:adblockinc/ext/adblockplus/adblockplusui.git";
const ABP_TAG = "next";
const DIST_DIR = "./dist-abp";
const SNIPPETS_DIR = `${TEMP_DIR}/snippets`;
const ABP_DIR = `${TEMP_DIR}/adblockplusui`;

if (REMOTE)
  printHeadline("Building ABP with @eyeo/snippets & @eyeo/mlaf");
else
  printHeadline("Building ABP with @eyeo/snippets & local mlaf");

printProgress("Cleaning up");
cleanUpTemp();
createTemp();

printProgress("Cloning projects");
exec("git", ["clone", "--single-branch", "--branch", SNIPPETS_TAG, SNIPPETS_ORIGIN, SNIPPETS_DIR]);
exec("git", ["clone", "--single-branch", "--branch", ABP_TAG, ABP_ORIGIN, ABP_DIR]);

printProgress("Patching snippets");
for (let file of [`${SNIPPETS_DIR}/rollup/ml.config.js`, `${SNIPPETS_DIR}/rollup/chromium-all.config.js`])
  fs.writeFileSync(file, fs.readFileSync(file, {encoding: "utf8"}).replaceAll("beautify(),", ""));

printProgress("Building snippets");
exec("npm", ["i", MLAF_ORIGIN], SNIPPETS_DIR);
exec("npm", ["run", "build"], SNIPPETS_DIR);

// Remove node_modules when building from remote source to simulate a "real" development environment
if (REMOTE)
  exec("rm", ["-rf", "node_modules"], SNIPPETS_DIR);

printProgress("Building ABP");
exec("npm", ["i", "../snippets"], ABP_DIR);
exec("npm", ["run", "build:release", "chrome", "-m", 2], ABP_DIR);

printProgress("Copying files");
fs.rmSync(DIST_DIR, {recursive: true, force: true});
fs.mkdirSync(DIST_DIR);
exec("mv", [`${ABP_DIR}/dist/release/*`, DIST_DIR]);

printProgress("Cleaning up");
cleanUpTemp();
