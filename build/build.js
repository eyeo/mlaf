import fs from "fs";
import {printHeadline, printProgress, exec, cleanUpTemp, createTemp, TEMP_DIR} from "./common.js";

const MLBROWSERUTILS_ORIGIN = "git@gitlab.com:eyeo/machine-learning/mlbrowserutils.git";
const MLBROWSERUTILS_TAG = "main";

const LIB_DIR = "./lib";
const MLBROWSERUTILS_DIST_DIR = `${TEMP_DIR}/dist`;

// Check for build arguments:
// `npm run build [-- [--mlbu-repo <REPO_URL>] [--mlbu-tag <TAG>] [--mlbu-dir <PATH>] [--mlbu-no-test] [--mlbu-no-build]]`
// Examples:
// `npm run build -- --mlbu-repo ../mlbrowserutils/ --mlbu-tag feature_branch` for a local feature branch
// `npm run build -- --mlbu-repo https://example.com/repo.git --mlbu-tag tag_name` for a remote tag
// `npm run build -- --mlbu-dir /path/to/mlbu` for copying a local/unstaged working directory
// `npm run build -- --mlbu-no-test` will omit `npm run test` on mlbrowserutils
// `npm run build -- --mlbu-no-build` will omit `npm run build` on mlbrowserutils
let mlbrowserutilsBuild = !process.argv || !process.argv.includes("--mlbu-no-build");
let mlbrowserutilsTest = !process.argv || !process.argv.includes("--mlbu-no-test");
let mlbrowserutilsOrigin = process.argv && process.argv.includes("--mlbu-repo") ?
  process.argv[process.argv.indexOf("--mlbu-repo") + 1] : MLBROWSERUTILS_ORIGIN;
let mlbrowserutilsTag = process.argv && process.argv.includes("--mlbu-tag") ?
  process.argv[process.argv.indexOf("--mlbu-tag") + 1] : MLBROWSERUTILS_TAG;
let mlbrowserutilsDir = process.argv && process.argv.includes("--mlbu-dir") ?
  process.argv[process.argv.indexOf("--mlbu-dir") + 1] : "";

printHeadline("Building @eyeo/mlaf");

// Clean up
printProgress("Cleaning up");
cleanUpTemp();
createTemp();

// Clone & build mlbrowserutils
if (mlbrowserutilsDir) {
  // If a directory is given, copy this instead of cloning mlbrowserutils
  // Used by mlbrowsuerutils `npm build-abp`
  printProgress(`Copying mlbrowserutils from ${mlbrowserutilsDir}`);
  exec("rsync", ["-avq", `${mlbrowserutilsDir}/`, TEMP_DIR, "--exclude", "temp-build-abp", "--exclude", "node_modules"]);
}
else {
  // If no directory is given, clone the provided/default repo
  printProgress(`Cloning mlbrowserutils from ${mlbrowserutilsTag}@${mlbrowserutilsOrigin}`);
  exec("git", ["clone", mlbrowserutilsOrigin, "--branch", mlbrowserutilsTag, "--single-branch", "-c", "advice.detachedHead=false", TEMP_DIR]);
}
printProgress("Installing mlbrowserutils' dependencies");
exec("npm", ["i"], TEMP_DIR);
if (mlbrowserutilsBuild) {
  printProgress("Building mlbrowserutils");
  exec("npm", ["run", "build"], TEMP_DIR);
}
if (mlbrowserutilsTest) {
  printProgress("Testing mlbrowserutils");
  exec("npm", ["run", "test"], TEMP_DIR);
}

// Populate lib
printProgress("Copying mlbrowserutils dist");
fs.rmSync(LIB_DIR, {recursive: true, force: true});
fs.cpSync(MLBROWSERUTILS_DIST_DIR, LIB_DIR, {recursive: true});

// Cleanup
printProgress("Cleaning up");
cleanUpTemp();
