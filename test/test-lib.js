/*
 * This file is part of eyeo's mlaf module (@eyeo/mlaf),
 * Copyright (C) 2006-present eyeo GmbH
 *
 * @eyeo/mlaf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * @eyeo/mlaf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with @eyeo/mlaf.  If not, see <http://www.gnu.org/licenses/>.
 */
import assert from "assert";
import "./env.js";
import {graphMLUtil as graphMLUtilMin, tfjsPartial as tfjsPartialMin, hideIfClassifies as hideIfClassifiesMin, service as serviceMin} from "@eyeo/mlaf";
import {graphMLUtil as graphMLUtilSrc, tfjsPartial as tfjsPartialSrc, hideIfClassifies as hideIfClassifiesSrc, service as serviceSrc} from "@eyeo/mlaf/src";

const COL_CLEAR = "\u001b[0m";
const COL_CYAN = "\u001b[36m";
const MUTABLE_SHELL = "moveCursor" in process.stdout;

// Test ensures a sane environment containing all
// expected exports.

// Modules, exports and members to be tested
let members = new Map([
  [graphMLUtilMin, "Module"],
  [tfjsPartialMin, "Module"],
  [serviceMin, "Module"],
  [graphMLUtilSrc, "Module"],
  [tfjsPartialSrc, "Module"],
  [serviceSrc, "Module"],
  [hideIfClassifiesMin, "function"],
  [hideIfClassifiesSrc, "function"],
  [graphMLUtilMin.digestPrediction, "function"],
  [graphMLUtilMin.domToGraph, "function"],
  [graphMLUtilMin.inference, "function"],
  [graphMLUtilMin.loadBundledModel, "function"],
  [graphMLUtilMin.loadModel, "function"],
  [graphMLUtilMin.predict, "function"],
  [graphMLUtilMin.preprocessGraph, "function"],
  [graphMLUtilSrc.digestPrediction, "function"],
  [graphMLUtilSrc.domToGraph, "function"],
  [graphMLUtilSrc.inference, "function"],
  [graphMLUtilSrc.loadBundledModel, "function"],
  [graphMLUtilSrc.loadModel, "function"],
  [graphMLUtilSrc.predict, "function"],
  [graphMLUtilSrc.preprocessGraph, "function"],
  [tfjsPartialMin.cast, "function"],
  [tfjsPartialMin.enableProdMode, "function"],
  [tfjsPartialMin.env, "function"],
  [tfjsPartialMin.loadGraphModel, "function"],
  [tfjsPartialMin.oneHot, "function"],
  [tfjsPartialMin.stack, "function"],
  [tfjsPartialMin.sum, "function"],
  [tfjsPartialMin.tensor, "function"],
  [tfjsPartialMin.tidy, "function"],
  [tfjsPartialMin.unstack, "function"],
  [tfjsPartialMin.zeros, "function"],
  [tfjsPartialSrc.cast, "function"],
  [tfjsPartialSrc.enableProdMode, "function"],
  [tfjsPartialSrc.env, "function"],
  [tfjsPartialSrc.loadGraphModel, "function"],
  [tfjsPartialSrc.oneHot, "function"],
  [tfjsPartialSrc.stack, "function"],
  [tfjsPartialSrc.sum, "function"],
  [tfjsPartialSrc.tensor, "function"],
  [tfjsPartialSrc.tidy, "function"],
  [tfjsPartialSrc.unstack, "function"],
  [tfjsPartialSrc.zeros, "function"],
  [serviceMin.MESSAGE_INFERENCE_SUFFIX, "string"],
  [serviceMin.MESSAGE_PREFIX, "string"],
  [serviceMin.MESSAGE_PREPARE_SUFFIX, "string"],
  [serviceMin.digestMessage, "function"],
  [serviceMin.errors, "object"],
  [serviceMin.messageListener, "function"],
  [serviceSrc.MESSAGE_INFERENCE_SUFFIX, "string"],
  [serviceSrc.MESSAGE_PREFIX, "string"],
  [serviceSrc.MESSAGE_PREPARE_SUFFIX, "string"],
  [serviceSrc.digestMessage, "function"],
  [serviceSrc.errors, "object"],
  [serviceSrc.messageListener, "function"]
]);

process.stdout.write(`${COL_CYAN}---------------------------\n`);
process.stdout.write(`${COL_CYAN}=== ${COL_CLEAR}MLAF: Running Tests ${COL_CYAN}===\n`);
process.stdout.write(`${COL_CYAN}---------------------------\n`);

// Run tests
let numTests = 0;
for (let [member, memberType] of members.entries()) {
  // Test type
  if (memberType === "Module")
    assert(member[Symbol.toStringTag] === memberType, `${member[Symbol.toStringTag]} is not a ${memberType}`);
  else
    assert(typeof member === memberType, `${member.name || member} is not a ${memberType}`);

  // Test content sanity
  if (memberType === "string")
    assert(!!member, `${member} is empty`);
  else if (memberType === "object")
    assert(Object.keys(member).length, `${member} is empty`);

  if (MUTABLE_SHELL) {
    if (numTests > 0) {
      for (let n = 0; n < 3; n++) {
        process.stdout.moveCursor(0, n > 0 ? -1 : null);
        process.stdout.clearLine(0);
      }
      process.stdout.cursorTo(0);
    }
    process.stdout.write(`${COL_CYAN}Tests passed: ${COL_CLEAR}${++numTests}/${members.size}\n`);
    process.stdout.write(`${COL_CYAN}---------------------------\n`);
  }
}


