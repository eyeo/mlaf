/*
 * This file is part of eyeo's mlaf module (@eyeo/mlaf),
 * Copyright (C) 2006-present eyeo GmbH
 *
 * @eyeo/mlaf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * @eyeo/mlaf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with @eyeo/mlaf.  If not, see <http://www.gnu.org/licenses/>.
 */

const DEFAULT_GRAPH_CUTOFF = 500;

/**
 * Turn a dom element into a graph
 * @param {Object} bundle - Model bundle as a JSON object
 * @param {Object} target - Dom element to turn into graph
 * @returns {Promise.<Object>} Returns a JSON object representing target as a graph
 */
async function domToGraph(bundle, target, denoise = false) {
  return new Promise((resolve, reject) => {
    if (!bundle || !target)
      return reject();

    let config = bundle.config;
    let cutoff = config.cutoff || bundle.topology.graphml.nodes || DEFAULT_GRAPH_CUTOFF;

    // Pre-compute node names to speed up traversal
    config = config.filter(e => e.include);
    for (let node of config)
      node.groupName = Object.keys(node)[2];

    let _extract = (element, data, group, attribute) => {
      if (group === "attributes" && typeof element.attributes[attribute] !== "undefined")
        data.attributes[attribute] = element.attributes[attribute].value;
      else if (group === "style" && element.style[attribute])
        data.attributes.style[attribute] = element.style[attribute];
      else if (group === "css")
        data.cssSelectors = getComputedStyle(element).cssText || "";
    };

    let _traverse = element => {
      // Stop depth traversal if dimensions are zero while denoising
      // Done before cutoff so ignored nodes don't count towards the cutoff limit
      if (denoise && !element.clientWidth && !element.clientHeight)
        return;

      cutoff -= 1;
      // Stop traversal if cutoff is reached. Cascades to the remaining siblings
      if (cutoff < 0)
        return;

      let data = {
        tag: element.tagName,
        width: element.clientWidth,
        height: element.clientHeight,
        attributes: {
          style: {}
        },
        children: []
      };

      for (let node of config) {
        for (let feature of node[node.groupName].features) {
          for (let [featureId, featureContent] of Object.entries(feature)) {
            if ("names" in featureContent) {
              for (let featureName of featureContent.names) {
                for (let featureNameFragment of featureName.split("^"))
                  _extract(element, data, node.groupName, featureNameFragment);
              }
            }
            else {
              _extract(element, data, node.groupName, featureId);
            }
          }
        }
      }

      if (element.children) {
        for (let child of element.children) {
          let childGraph = _traverse(child);
          if (childGraph)
            data.children.push(childGraph);
        }
      }

      return data;
    };

    let graph = _traverse(target);
    resolve(graph);
  });
}

function parseArgs(args) {
  if (!args || !Array.isArray(args) || !args.length)
    return {};

  let selectors = [];
  let ret = {
    debug: false,
    frameonly: false,
    failsafe: false,
    denoise: false,
    silent: false,
    model: "",
    selector: "",
    subselector: ""
  };

  // Catch general options
  for (let arg of args) {
    if (arg && arg in ret)
      ret[arg] = true;
    else
      selectors.push(arg);
  }

  // Deconstruct rest into model, selector and subselector
  if (selectors.length < 2)
    return {};

  ret.model = selectors[0];
  selectors.splice(0, 1);

  // Handle complex selectors with spaces (`".foo bar" .baz`)
  if (selectors.length > 2 || selectors.some(e => e && e.startsWith("\"")))
    selectors = selectors.join(" ").split("\"").map(e => e.trim()).filter(e => e);

  ret.selector = selectors[0] || "";
  ret.subselector = selectors[1] || "";

  return ret;
}

function resolveSelectors(selectors, subselector) {
  let targetContainers = [document];
  selectors = $(selectors).split("..");
  let targets = [];

  $(selectors).forEach((selector, index) => {
    // Traverse to target's parent for every selector except for the first
    if (index) {
      targetContainers = $(targetContainers)
        .reduce(($arr, e) => (e && $(e).parentElement ? $arr.concat($(e).parentElement) : $arr), $([]));
    }
    // Execute selector and store nodes if valid
    targetContainers = $(targetContainers).reduce(($arr, e) => {
      if (!selector) {
        // If selector is blank, leave targetContainer in the array
        $arr.push(e);
      }
      else {
        // If selector isn't blank replace targetContainer with the selected nodes
        // Ignore results from illegal selectors
        try {
          $arr = $arr.concat(...$(e).querySelectorAll(selector));
        }
        catch (err) { /* no-op */ }
      }
      return $arr;
    }, $([]));
  });

  for (let targetContainer of targetContainers) {
    let inferenceTargets = [targetContainer];
    if (subselector) {
      try {
        inferenceTargets = $(targetContainer).querySelectorAll(subselector);
      }
      catch (err) { /* no-op */ }
    }
    // Store all targets in the inference backlog which is digested once the corresponding model
    // is loaded and on every subsequent page mutation.
    targets.push([targetContainer, inferenceTargets]);
  }
  return targets;
}

// Fall back to (_ => _) instead of $ if $ is undefined. This
// happens when running tests outside of the ABP-Snippets'
// artifact.
let {
  WeakSet: WeakSet$,
  MutationObserver: MutationObserver$
} = (typeof $ !== "undefined" ? $ : (_ => _))(window);

class Observer {
  constructor() {
    this.digestedElements = new WeakSet$();
    this.selector = "";
    this.subselector = "";
  }

  observe(selector, subselector, callback) {
    // Start observing the dom tree for changes
    this.selector = selector;
    this.subselector = subselector;
    this.callback = callback;
    this.elementObserver = new MutationObserver$(this.digest.bind(this));
    this.elementObserver.observe(document, {childList: true, subtree: true});
    this.digest();
  }

  stop() {
    // Stop snippet entirely
    if (this.elementObserver)
      this.elementObserver.disconnect();
  }

  digest() {
    // Resolve selectors to dom elements.
    // Will obey pseudo-:has() by performing parent traversal
    // in case `..` is present in the respective selector:
    //   `div.foo..:scope>.bar`
    // translates to
    //   `*:has(:scope>div.foo)>.bar`
    let targets = resolveSelectors(this.selector, this.subselector)
      .filter(([target]) => !this.digestedElements.has(target));
    // Only return new targets
    this.callback(targets);
    for (let [target] of targets)
      this.digestedElements.add(target);
  }
}

// Enable debug output.
// Can be turned on by adding "mldebug:" to the beginning of filters:
// domain.com$%$hide-if-classifies mldebug:div.someclass
let mode = false;

/**
 * Prints debug output to the console if debug mode is active.
 * @param {String} message - A message to be printed
 * @param {Boolean} error - A boolean indicating whether the message is an error
 * @param {...} - Any additional parameters will be piped through console.log.
 */
function print$1(message, error = false, ...additionalMessages) {
  if (mode) {
    // eslint-disable-next-line no-console
    console.log(
      "%cMLDBG \u2665 %c| %s%c |",
      "color:cyan",
      error ? "color:red" : "color:inherit",
      message,
      "color:inherit",
      ...additionalMessages
    );
  }
}

/**
 * Toggle debug mode on/off.
 * Off: debug.print() is silent
 * On: debug.print() will print to console
 * @param {String} message - A message to be printed
 * @param {Boolean} error - A boolean indicating whether the message is an error
 * @param {...} - Any additional parameters will be piped through console.log.
 */
function toggle(val) {
  mode = !!val;
}

let data = {
  nodeCount: 0,
  organicCount: 0,
  adCount: 0,
  aaCount: 0,
  times: []
};

function set(prediction = false, allowlisted = false, time) {
  if (mode) {
    if (!data.nodeCount) {
      $(document).head.insertAdjacentHTML("beforeend", `
        <style>body::before { display: block; position: fixed; pointer-events: none; top: 0; left: 0; z-index: 999999; background-color: rgba(0, 0, 0, 0.7); padding: 5px; content: "ad:\\9\\9 " var(--dbg-ad) "\\A nad:\\9\\9 " var(--dbg-nad) "\\A aa:\\9\\9 " var(--dbg-aa) "\\A time:\\9 " var(--dbg-t); font-size: 10px; white-space: pre-wrap; color: #fff; }</style>
      `);
    }
  }
  data.nodeCount++;
  if (allowlisted)
    data.aaCount++;
  else if (prediction)
    data.adCount++;
  else
    data.organicCount++;
  if (time)
    data.times.push(time);
}

function print() {
  if (mode) {
    $(document).body.style.setProperty("--dbg-ad", `"${data.adCount}"`);
    $(document).body.style.setProperty("--dbg-nad", `"${data.organicCount}"`);
    $(document).body.style.setProperty("--dbg-aa", `"${data.aaCount}"`);
    $(document).body.style.setProperty("--dbg-t", `"${data.times.reduce((s, v, i) => !i || i % 3 ? s += v + "ms " : s += "\\A\\9\\9 " + v + "ms ", "")}"`);
  }
}

const MESSAGE_PREFIX = "ML:";
const MESSAGE_PREPARE_SUFFIX = "prepare";
const MESSAGE_INFERENCE_SUFFIX = "inference";
const errors = {
  UNKNOWN_REQUEST: 1,
  MISSING_REQUEST_DATA: 2,
  UNKNOWN_MODEL: 3,
  MISSING_INFERENCE_DATA: 4,
  INFERENCE_FAILED: 5,
  MODEL_INSTANTIATION_FAILED: 6,
  MISSING_ENVIRONMENTAL_SUPPORT: 7
};

const IN_FRAME = window.self !== window.top;
// Timeout in MS before request to service worker is cancelled
const SERVICE_WORKER_TIMEOUT = 10000;

// Fall back to (_ => _) instead of $ if $ is undefined. This
// happens when running tests outside of the ABP-Snippets'
// artifact.
let {
  Map: Map$
} = (typeof $ !== "undefined" ? $ : (_ => _))(window);
let modelConfigs = new Map$();
let globallyAllowlisted = false;

/**
 * Hides any HTML element that is classified as an ad by a machine learning
 * graph model.
 * @param {Array} arguments A snippet filter split by spaces. Filter options:
 *   domain.com#$#hide-if-classifies
 *     debug // Optional, turns on debug output
 *     frameonly // Optional, runs the snippets only inside frames
 *     failsafe // Optional, turns off the snippet if ad/nad ratio is too skewed
 *     denoise // Optional, will cause the snippet to denoise target elements
 *     silent // Optional, will cause the snippet to run inference/telemetry
 *       but won't filter ads (same as `debug` but without annotation)
 *     model-1.0.0 // Required, name of the model to be used
 *     div.foo // Required, target selector. Must be wrapped in double quotes if
 *       it contains spaces
 *     span.bar // Optional, target subselector. If present, inference will be run
 *       on the subselector instead of the entire target. Must be wrapped in
 *       double quotes if it contains spaces
 *   Examples:
 *     domain.com#$#hide-if-classifies model-1.0.0 iframe
 *     domain.com#$#hide-if-classifies debug model-1.0.0 div.ad iframe
 *     domain.com#$#hide-if-classifies debug failsafe frameonly denoise
 *       model-1.0.0 "div div.ad" "ul span.header"
 *   Selectors can contain `..` to select the selected element's parent. E.g.:
 *     div.someclass..span
 *       Which will search for `span` within the parent of `div.someclass`.
 *     div.someclass....:scope>div
 *       Which will search for divs as direct ancestors of two parents up
 *       from `div.someclass`.
 */
function hideIfClassifies(...args) {
  let {
    debug: debugMode,
    frameonly,
    failsafe,
    denoise,
    silent,
    model,
    selector,
    subselector
  } = parseArgs(args || []);

  toggle(debugMode);

  if (typeof chrome === "undefined" || !chrome.runtime || !chrome.runtime.sendMessage)
    return print$1("environmental support", false);

  if (!model || !selector)
    return print$1("Invalid filter", true);

  // If the `frameonly` filter flag is set, stop executing the snippet
  // if it's run in the root document
  if (frameonly && !IN_FRAME)
    return;

  if (!IN_FRAME)
    print$1(`Filter hit for ${model}: ${selector} ${subselector}`);

  let observer = new Observer();
  let raceWinnerCallback = raceWinner(
    "hide-if-graph-matches",
    () => observer.stop()
  );

  let getModelConfig = () => {
    // Return a promise which waits for the call to the service worker
    // to finish. Resolves to the model config. Promise is stored in a
    // map so other filter hits using the same model won't request the
    // configuration multiple times.
    if (modelConfigs.has(model))
      return modelConfigs.get(model);

    print$1(`Preparing worker with model ${model}`);
    // Store a promise resolving to the config so subsequent
    // requests to the same model can reuse the initial request.
    let configRequest = new Promise((resolve, reject) => {
      let responseTimeout = setTimeout(() => {
        reject(`Worker preparation with ${model} failed: service worker timeout`);
      }, SERVICE_WORKER_TIMEOUT);
      sendMessage({
        type: MESSAGE_PREFIX + MESSAGE_PREPARE_SUFFIX,
        debug: mode,
        model
      }, response => {
        clearTimeout(responseTimeout);
        if (response && "config" in response) {
          print$1(`Received config for ${model}`, false, "config:", response.config);
          response.config.cutoff = response.cutoff;
          resolve(response.config);
        }
        else {
          reject(`Worker preparation with ${model} failed`);
        }
      });
    });
    // Silently catch error in case the request is never digested
    configRequest.catch(err => { /* no-op */ });
    modelConfigs.set(model, configRequest);
    return configRequest;
  };

  let inference = (graph, targetContainer) => {
    // If failsafe filter flag is set, check if the ad ratio is one-sided after the first 10 inference runs
    if (failsafe && data.nodeCount >= 10 && data.adCount / data.nodeCount >= 1) {
      print$1("Ad to non-ad ratio is 100%/0%. Stopping inference.", true);
      return observer.stop();
    }

    print$1(`Requesting inference with ${model}`, false, "graph:", graph);
    sendMessage({
      type: MESSAGE_PREFIX + MESSAGE_INFERENCE_SUFFIX,
      debug: mode,
      model,
      graph
    }, response => {
      if (response && "prediction" in response && typeof response.prediction === "boolean") {
        print$1(`Received ${response.prediction} inference results with ${model}`, false, "graph:", graph);
        // Allowlisting flag is set globally, shared by all hide-if-classifies instances.
        if (response.allowlisted && !globallyAllowlisted)
          globallyAllowlisted = true;
        set(response.prediction, response.allowlisted, response.digestTime - response.startTime);
        // Check if another snippet has set the flag irregardless of the responses allowlisting status.
        // Continue running in debug mode in order to annotate content.
        if (globallyAllowlisted && !mode)
          return observer.stop();

        if (response.prediction && !mode && !silent) {
          raceWinnerCallback();
          hideElement(targetContainer);
        }
        else if (mode) {
          if (response.allowlisted)
            $(targetContainer).style.border = "3px solid #00ffff";
          else if (response.prediction)
            $(targetContainer).style.border = "3px solid #ff0000";
          else if (!response.prediction)
            $(targetContainer).style.border = "3px solid #00ff00";
          print();
        }
      }
      else {
        print$1(`Inference with ${model} failed`, true, "graph:", graph, "response:", response);
        // Stop sending more inference requests if the service worker fails to
        // run inference. This can happen due to missing OffscreenCanvas support
        // or lack of hardware acceleration.
        if ("error" in response && (response.error === errors.INFERENCE_FAILED ||
          response.error === errors.MISSING_ENVIRONMENTAL_SUPPORT))
          observer.stop();
      }
    });
  };

  let sendMessage = (data, callback) => {
    // Stop polling backend if allowlisted element was found.
    // However, if in debug mode, keep annotating elements.
    if (!globallyAllowlisted)
      chrome.runtime.sendMessage(data, callback);
    else if (mode)
      callback({prediction: false, allowlisted: true});
  };

  let observerCallback = targets => {
    // Callback will be called every time nodes are added to the backlog

    // Allow attempts to digest the backlog even if it's empty as long as the snippet
    // is invoked on the topmost frame. This improves the service worker boot up time.
    if ((!targets || !targets.length) && IN_FRAME)
      return getModelConfig();

    getModelConfig().then(modelConfig => {
      if (!modelConfig)
        return Promise.reject(`Config file for ${model} corrupted`);
      for (let [targetContainer, inferenceTargets] of targets) {
        for (let inferenceTarget of inferenceTargets) {
          print$1(`Preparing inference with ${model}`, false, "target:", targetContainer);
          // If denoising is enabled but the returned graph is empty, fall back to non-denoising.
          // This will catch edge cases where root nodes have zero dimensions
          domToGraph({config: modelConfig}, inferenceTarget, denoise)
            .then(graph => denoise && !graph ? domToGraph({config: modelConfig}, inferenceTarget, false) : graph)
            .then(graph => inference(graph, targetContainer))
            .catch(e => print$1(`domToGraph failed with error "${e}"`, true, "target:", targetContainer));
        }
      }
    }).catch(err => {
      print$1(err, true);
      observer.stop();
    });
  };

  observer.observe(selector, subselector, observerCallback);
}

export { hideIfClassifies };
